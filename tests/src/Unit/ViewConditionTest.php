<?php

namespace Drupal\Tests\trophy\Unit;

use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Entity\Query\QueryInterface;
use Drupal\Core\Field\FieldItemList;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Tests\UnitTestCase;
use Drupal\trophy\Entity\Trophy;
use Drupal\trophy\Entity\TrophyViewCondition;
use Drupal\trophy\Plugin\TrophyCondition\ViewCondition;
use Prophecy\Argument;
use Prophecy\Prediction\CallTimesPrediction;
use Prophecy\Promise\CallbackPromise;
use Psr\Log\LoggerInterface;

/**
 * Test description.
 *
 * @group trophy
 */
class ViewConditionTest extends UnitTestCase {

  /**
   * The Entity Storage Interface.
   *
   * @var ObjectProphecy
   */
  protected $entityStorage;

  /**
   * The ID of the type of the entity under test.
   *
   * @var \Drupal\trophy\Plugin\TrophyCondition\ViewCondition
   */
  protected $plugin;

  /**
   * Existed Trophy's date.
   *
   * @var int
   */
  protected $lowest = 0;

  /**
   * Trophy if exist or not.
   *
   * @var int
   */
  protected $isTrophyExist = TRUE;

  /**
   * Returns lowest data.
   */
  public function getLowest() {
    return $this->lowest;
  }

  /**
   * Returns exist trophy.
   */
  public function getExistTrophy() {
    return $this->isTrophyExist ? [1] : [];
  }

  /**
   * Set up the test.
   */
  protected function setUp(): void {
    parent::setUp();

    $container = new ContainerBuilder();

    $languageManager = $this->prophesize(LanguageManagerInterface::class);
    $languageManager->getCurrentLanguage()->willReturn($this->prophesize(LanguageInterface::class)->reveal());
    $container->set('language_manager', $languageManager->reveal());
    $container->setParameter('app.root', $this->root);

    \Drupal::setContainer($container);

    $this->entityStorage = $this->prophesize(EntityStorageInterface::class);

    $query = $this->prophesize(QueryInterface::class);
    $query->condition(Argument::any(), Argument::any())->willReturn($query);
    $query->sort(Argument::any(), Argument::any())->willReturn($query);
    $query->range(Argument::any(), Argument::any())->willReturn($query);
    $query->execute()->will(new CallbackPromise([$this, 'getExistTrophy']));

    $this->entityStorage->getQuery(Argument::any())->willReturn($query);

    $fieldItemList = $this->prophesize(FieldItemList::class);
    $fieldItemList->getValue()->will(new CallbackPromise([$this, 'getLowest']));

    $trophy = $this->prophesize(Trophy::class);
    $trophy->get('acquisition_date')->willReturn($fieldItemList);

    $this->entityStorage->load(1)->willReturn($trophy);

    $entityTypeManager = $this->prophesize(EntityTypeManager::class);
    $entityTypeManager->getStorage('trophy')->willReturn($this->entityStorage);

    $logger = $this->prophesize(LoggerInterface::class);
    $date_formatter = $this->prophesize(DateFormatterInterface::class);
    $this->plugin = new ViewCondition([], 'view_condition', [], $entityTypeManager->reveal(), $logger->reveal(), $date_formatter->reveal());

  }

  /**
   * Date abbreviations.
   *
   * @var string[]
   */
  private $dateAbreviations = [
    'h' => 'hour',
    'd' => 'day',
    'w' => 'week',
    'm' => 'month',
    'y' => 'year',
  ];

  /**
   * Returns readable configuration.
   */
  private function getReadableConfig($config, $expected, $arg) {
    $from = date('m/d/Y H:i:s', $config['from']);
    $to = date('m/d/Y H:i:s', $config['to']);
    $target = $config['target'];
    $granularity = $this->dateAbreviations[$config['granularity']];
    $require_consistency = $config['require_consistency'] ? 'consistency required' : 'consistency not required';
    $recursively = $config['recursively'] ? 'each' : 'once';
    $nodes = implode(', ', $config['nodes']);
    return "[$require_consistency] Within $from to $to get a point $recursively $granularity on nodes [$nodes] and check if reaches $target [$expected Trophy CREATED]$arg";
  }

  /**
   * Data used to test different cases.
   */
  public function defaultDataProvider() {
    $time = 1681344000;
    $dateTime = new \DateTime("@$time");

    $defaultConfiguration = [
      'from' => $time,
      'to' => $time + (86400 * 3),
      'target' => 5,
      'granularity' => 'h',
      'require_consistency' => FALSE,
      'recursively' => TRUE,
      'nodes' => [],
    ];

    $viewConditionEntities = [];

    for ($i = 0; $i < 200; $i++) {
      $viewConditionEntities['sequential_hourly'][] = [
        $i, 1, $time + (3600 * $i),
      ];
      if ($i % 2 == 1) {
        $viewConditionEntities['spaced_hourly'][] = [
          $i, 1, $time + (3600 * $i),
        ];
      }
    }

    for ($i = 0; $i < 150; $i++) {
      $viewConditionEntities['sequential_daily'][] = [
        $i, 1, $time + (86400 * $i),
      ];

      if ($i % 2 == 1) {
        $viewConditionEntities['spaced_daily'][] = [
          $i, 1, $time + (86400 * $i),
        ];
      }
    }

    for ($i = 0; $i < 50; $i++) {
      $viewConditionEntities['sequential_monthly'][] = [
        $i, 1, $dateTime->getTimestamp(),
      ];

      if ($i % 2 == 1) {
        $viewConditionEntities['spaced_monthly'][] = [
          $i, 1, $dateTime->getTimestamp(),
        ];
      }
      $dateTime->add(new \DateInterval("P1M"));
    }

    $datas = [];

    // Within a days, 4 trophy must created due to target is 5 & 24/5=4.8.
    // 0.8 is enough for crossed hours. Check the next data for more.
    $conf = [...$defaultConfiguration, 'to' => $time + (86400)];
    $expected = 4;
    $datas[$this->getReadableConfig($conf, $expected, ' on a sequential_hourly dataset')] = [
      $conf,
      $viewConditionEntities['sequential_hourly'],
      0,
      $expected,
      FALSE,
    ];

    // Within 3 days, 12 trophy must be created due to;
    // Target is 5 & 24*3=72 & 72/5=14.4.
    // But algorithm does not include same hour range twice. For example;
    // 1 day has 24 hours ...../...../...../...../ (/) is for crossed times.
    // Which is equal 3600 if granularity is h, 86400 for d and so on.
    $conf = $defaultConfiguration;
    $expected = 12;
    $datas[$this->getReadableConfig($conf, $expected, ' on a sequential_hourly dataset')] = [
      $conf,
      $viewConditionEntities['sequential_hourly'],
      0,
      $expected,
      FALSE,
    ];

    // Within 3 days, 18 trophy must created due to;
    // Target is 3 & 24*3=72 & 72-18=54 & 54/3=18.
    $conf = [...$defaultConfiguration, 'target' => 3];
    $expected = 18;
    $datas[$this->getReadableConfig($conf, $expected, ' on a sequential_hourly dataset')] = [
      $conf,
      $viewConditionEntities['sequential_hourly'],
      0,
      $expected,
    ];

    // Within 3 days, 7 trophy must created due to;
    // Target is 10 & 24*3=72 & 72-6=66 & 66/10=6.6.
    $conf = [...$defaultConfiguration, 'target' => 10];
    $expected = 6;
    $datas[$this->getReadableConfig($conf, $expected, ' on a sequential_hourly dataset')] = [
      $conf,
      $viewConditionEntities['sequential_hourly'],
      0,
      $expected,
    ];

    // Within 7 days, 10 trophy must created due to;
    // Target is 16 & 24*7=169 & 169-10+1=160 & 160/16=10.
    // This test made me notice that the last part are also handled.
    // More tests are good to cover as many corner cases as possible.
    $conf = [
      ...$defaultConfiguration,
      'target' => 16,
      'require_consistency' => TRUE,
      'to' => $time + (86400 * 7),
    ];
    $expected = 10;
    $datas[$this->getReadableConfig($conf, $expected, ' on a sequential_hourly datasetxxxa')] = [
      $conf,
      $viewConditionEntities['sequential_hourly'],
      0,
      $expected,
    ];

    // Within 7 days, 10 trophy must created due to;
    // Target is 16 & 24*7=169 & 169-10+1=160 & 160/16=10.
    // The result is 10/2=5 due to the dataset being spaced.
    $conf = [
      ...$defaultConfiguration,
      'target' => 16,
      'require_consistency' => FALSE,
      'to' => $time + (86400 * 7),
    ];
    $expected = 5;
    $datas[$this->getReadableConfig($conf, $expected, ' on a spaced_hourly dataset')] = [
      $conf,
      $viewConditionEntities['spaced_hourly'],
      0,
      $expected,
    ];

    // Within 7 days, 10 trophy must created due to;
    // Target is 16 & 24*7=169 & 169-10+1=160 & 160/16=10.
    // But the dataset is spaced due to spaces there is no consistency.
    $conf = [
      ...$defaultConfiguration,
      'target' => 16,
      'require_consistency' => TRUE,
      'to' => $time + (86400 * 7),
    ];
    $expected = 0;
    $datas[$this->getReadableConfig($conf, $expected, ' on a spaced_hourly dataset')] = [
      $conf,
      $viewConditionEntities['spaced_hourly'],
      0,
      $expected,
    ];

    // Within 7 days, 3 trophy must created due to;
    // Target is 2 and it checks daily *[1-2] [3-4] [5-6] so on.
    // Note: from date is Apr 13 00:00:00 and to is Apr 19 00:00:00.
    $conf = [
      ...$defaultConfiguration,
      'target' => 2,
      'granularity' => 'd',
      'require_consistency' => TRUE,
      'to' => $time + (86400 * 6),
    ];
    $expected = 3;
    $datas[$this->getReadableConfig($conf, $expected, ' on a sequential_hourly dataset')] = [
      $conf,
      $viewConditionEntities['sequential_hourly'],
      0,
      $expected,
    ];

    // Within 7 days, 1 trophy must created due to recursively is false.
    // It does not create any more because there is already one!
    // Note: from date is Apr 13 00:00:00 and to is Apr 19 00:00:00.
    $conf = [
      ...$defaultConfiguration,
      'target' => 1,
      'granularity' => 'd',
      'recursively' => FALSE,
      'to' => $time + (86400 * 6),
    ];
    $expected = 1;
    $datas[$this->getReadableConfig($conf, $expected, ' on a sequential_hourly dataset')] = [
      $conf,
      $viewConditionEntities['sequential_hourly'],
      0,
      $expected,
      FALSE,
    ];

    // Within 6 days, 1 trophy must created due to;
    // Target is 7, and both the first and last are included.
    // Note: from date is Apr 13 00:00:00 and to is Apr 19 00:00:00.
    $conf = [
      ...$defaultConfiguration,
      'target' => 7,
      'granularity' => 'd',
      'require_consistency' => TRUE,
      'to' => $time + (86400 * 6),
    ];
    $expected = 1;
    $datas[$this->getReadableConfig($conf, $expected, ' on a sequential_hourly dataset')] = [
      $conf,
      $viewConditionEntities['sequential_hourly'],
      0,
      $expected,
    ];

    // Within 6 days, any trophy must not be created due to;
    // Target 7 and to (last) date is 1 second extracted.
    // It simulates visitors that do not visit the next day yet.
    // Note: from date is Apr 13 00:00:00 and to is Apr 19 00:00:00.
    $conf = [
      ...$defaultConfiguration,
      'target' => 7,
      'granularity' => 'd',
      'require_consistency' => TRUE,
      'to' => $time + (86400 * 6) - 1,
    ];
    $expected = 0;
    $datas[$this->getReadableConfig($conf, $expected, ' on a sequential_daily dataset')] = [
      $conf,
      $viewConditionEntities['sequential_daily'],
      0,
      $expected,
    ];

    // Within 30 days, 4 trophy must created due to;
    // Target is 5 & 30-4=26 & 26/5=5.2.
    $conf = [
      ...$defaultConfiguration,
      'target' => 5,
      'granularity' => 'd',
      'require_consistency' => TRUE,
      'to' => $time + (86400 * 30),
    ];
    $expected = 5;
    $datas[$this->getReadableConfig($conf, $expected, ' on a sequential_daily dataset')] = [
      $conf,
      $viewConditionEntities['sequential_daily'],
      0,
      $expected,
    ];

    // Within ~3 years, 1 trophy must created due to target is 12 and it counts monthly.
    // It does not create any more because there is already one.
    $conf = [
      ...$defaultConfiguration,
      'target' => 12,
      'granularity' => 'm',
      'recursively' => FALSE,
      'require_consistency' => TRUE,
      'to' => $time + (315360000 * 3),
    ];
    $expected = 1;
    $datas[$this->getReadableConfig($conf, $expected, ' on a sequential_monthly dataset')] = [
      $conf,
      $viewConditionEntities['sequential_monthly'],
      0,
      $expected,
      FALSE,
    ];

    // Within ~3 years, 16 trophy must created due to target is 3 and it counts monthly. !!!
    // It does not create any more because there is already one.
    $conf = [
      ...$defaultConfiguration,
      'target' => 3,
      'granularity' => 'm',
      'recursively' => TRUE,
      'require_consistency' => TRUE,
      'to' => $time + (315360000 * 3),
    ];
    $expected = 16;
    $datas[$this->getReadableConfig($conf, $expected, ' on a sequential_monthly dataset')] = [
      $conf,
      $viewConditionEntities['sequential_monthly'],
      0,
      $expected,
      FALSE,
    ];

    // It must not create any trophy due to consistency does not meet.
    $conf = [
      ...$defaultConfiguration,
      'target' => 12,
      'granularity' => 'm',
      'recursively' => FALSE,
      'require_consistency' => TRUE,
      'to' => $time + (315360000 * 3),
    ];
    $expected = 0;
    $datas[$this->getReadableConfig($conf, $expected, ' on a spaced_monthly dataset')] = [
      $conf,
      $viewConditionEntities['spaced_monthly'],
      0,
      $expected,
      FALSE,
    ];

    // Within ~3 years, 1 trophy must created due to;
    // Target is 12 and it counts monthly.
    // Even if visitors visits bimonthly because it not check consistency.
    // It does not create any more because
    // There is already one and 36/2=18 & 18-12=6 & 6<12.
    $conf = [
      ...$defaultConfiguration,
      'target' => 12,
      'granularity' => 'm',
      'recursively' => FALSE,
      'require_consistency' => FALSE,
      'to' => $time + (315360000 * 3),
    ];
    $expected = 1;
    $datas[$this->getReadableConfig($conf, $expected, ' on a spaced_monthly dataset')] = [
      $conf,
      $viewConditionEntities['spaced_monthly'],
      0,
      $expected,
      FALSE,
    ];

    // It must not create any trophy due to;
    // Bimonthly dataset and consistency requirement.
    $conf = [
      ...$defaultConfiguration,
      'target' => 12,
      'granularity' => 'm',
      'recursively' => FALSE,
      'require_consistency' => TRUE,
      'to' => $time + (315360000 * 3),
    ];
    $expected = 0;
    $datas[$this->getReadableConfig($conf, $expected, ' on a spaced_monthly dataset')] = [
      $conf,
      $viewConditionEntities['spaced_monthly'],
      0,
      $expected,
      FALSE,
    ];

    return $datas;
  }

  /**
   * Tests View Condition Process Method.
   *
   * @dataProvider defaultDataProvider
   */
  public function testViewConditionProcess(array $conf, array $entities, $lowest, $expected, $isTrophyExist = TRUE) {
    $this->plugin->setConfiguration([
      'data' => $conf,
    ]);

    $entities = array_map(function ($entities) {
      $entity = $this->prophesize(TrophyViewCondition::class);
      $entity->id()->willReturn($entities[0]);
      $entity->getOwnerId()->willReturn($entities[1]);
      $entity->getCreated()->willReturn($entities[2]);

      return $entity->reveal();
    }, $entities);

    $this->lowest = $lowest;
    $this->isTrophyExist = $isTrophyExist;

    $this->entityStorage->create(Argument::any())->should(new CallTimesPrediction($expected));
    $this->plugin->process($entities, 1, 1);
  }

}
