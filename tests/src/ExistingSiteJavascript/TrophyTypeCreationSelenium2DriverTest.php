<?php

// Use your module's testing namespace such as the one below.
namespace Drupal\Tests\trophy\ExampleSelenium2DriverTest;

use Drupal\trophy\Entity\TrophyType;
use weitzman\DrupalTestTraits\ExistingSiteSelenium2DriverTestBase;

/**
 * A browser test suitable for testing Ajax and client-side interactions.
 * @link https://gitlab.com/weitzman/drupal-test-traits/-/tree/2.x/tests
 */
class TrophyTypeCreationSelenium2DriverTest extends ExistingSiteSelenium2DriverTestBase {

  /**
   * Trophy Type creation selenium tests.
   */
  public function testContentCreation() {
    $web_assert = $this->assertSession();

    $admin = $this->createUser([], NULL, TRUE);
    $this->drupalLogin($admin);
    $this->drupalGet('/admin/structure/trophy/add');

    $page = $this->getCurrentPage();
    $page->fillField('point', '100');
    $page->fillField('label', 'Test Trophy Type');

    $machineNameButton = $page->find('css', '#edit-label-machine-name-suffix button');

    $page->waitFor(3, function () use ($machineNameButton) {
      return $machineNameButton->isVisible() === TRUE;
    });

    $machineNameButton->click();

    $page->fillField('name', 'test_trophy_type');
    $page->findButton('Save')->press();

    $web_assert->pageTextContains('Test Trophy Type');

    $this->markEntityForCleanup(TrophyType::load('test_trophy_type'));
  }

}
