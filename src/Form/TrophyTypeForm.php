<?php

namespace Drupal\trophy\Form;

use Drupal\Component\Utility\Unicode;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\trophy\ConfigurableTrophyConditionInterface;
use Drupal\trophy\TrophyConditionManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Trophy Type form.
 *
 * @property \Drupal\trophy\TrophyTypeInterface $entity
 */
class TrophyTypeForm extends EntityForm {

  /**
   * The trophy condition manager service.
   *
   * @var \Drupal\trophy\TrophyConditionManager
   */
  protected $trophyConditionManager;

  /**
   * Constructs an Trophy Type Form object.
   *
   * @param \Drupal\trophy\TrophyConditionManager $trophy_condition_manager
   *   The trophy condition manager service.
   */
  public function __construct(TrophyConditionManager $trophy_condition_manager) {
    $this->trophyConditionManager = $trophy_condition_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
          $container->get('plugin.manager.trophy_condition')
      );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $user_input = $form_state->getUserInput();
    $form = parent::form($form, $form_state);
    $form['#tree'] = TRUE;
    $form['#attached']['library'][] = 'trophy/admin';

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $this->entity->label(),
      '#description' => $this->t('Label for the trophy type.'),
      '#required' => TRUE,
    ];

    $form['name'] = [
      '#type' => 'machine_name',
      '#default_value' => $this->entity->id(),
      '#machine_name' => [
        'exists' => '\Drupal\trophy\Entity\TrophyType::load',
      ],
      '#disabled' => !$this->entity->isNew(),
    ];

    $form['badge'] = [
      '#type' => 'managed_file',
      '#title' => $this->t('Badge'),
      '#default_value' => $this->entity->badge(),
      '#upload_location' => 'private://trophy_badges/',
      '#upload_validators' => [
        'file_validate_extensions' => ['gif png jpg jpeg webp'],
      ],
    ];

    $form['point'] = [
      '#type' => 'number',
      '#title' => $this->t('Point'),
      '#default_value' => $this->entity->point(),
      '#required' => TRUE,
      '#description' => $this->t('Number, #type = number'),
    ];

    $form['status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
      '#default_value' => $this->entity->status(),
    ];

    $form['description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Description'),
      '#default_value' => $this->entity->get('description'),
      '#description' => $this->t('Description of the trophy type.'),
    ];

    // Build the list of existing trophy conditions for this trophy type.
    $form['conditions'] = [
      '#type' => 'table',
      '#header' => [
        $this->t('Conditions (OR)'),
        $this->t('Weight'),
        $this->t('Operations'),
      ],
      '#tabledrag' => [
      [
        'action' => 'order',
        'relationship' => 'sibling',
        'group' => 'trophy-condition-order-weight',
      ],
      ],
      '#attributes' => [
        'id' => 'trophy-conditions',
      ],
      '#empty' => $this->t('There are currently no conditions in this trophy type. Add one by selecting an option below.'),
      // Render conditions below parent elements.
      '#weight' => 5,
    ];

    foreach ($this->entity->getConditions() as $condition) {
      $key = $condition->getUuid();
      $form['conditions'][$key]['#attributes']['class'][] = 'draggable';
      $form['conditions'][$key]['#weight'] = isset($user_input['conditions']) ? $user_input['conditions'][$key]['weight'] : NULL;
      $form['conditions'][$key]['condition'] = [
        '#tree' => FALSE,
        'data' => [
          'label' => [
            '#plain_text' => $condition->label(),
          ],
        ],
      ];

      $form['conditions'][$key]['weight'] = [
        '#type' => 'weight',
        '#title' => $this->t('Weight for @title', ['@title' => $condition->label()]),
        '#title_display' => 'invisible',
        '#default_value' => $condition->getWeight(),
        '#attributes' => [
          'class' => ['trophy-condition-order-weight'],
        ],
      ];

      $links = [];
      $is_configurable = $condition instanceof ConfigurableTrophyConditionInterface;
      if ($is_configurable) {
        $links['edit'] = [
          'title' => $this->t('Edit'),
          'url' => Url::fromRoute(
              'trophy.condition_edit_form', [
                'trophy_type' => $this->entity->id(),
                'trophy_condition' => $key,
              ]
          ),
        ];
      }
      $links['delete'] = [
        'title' => $this->t('Delete'),
        'url' => Url::fromRoute(
            'trophy.condition_delete', [
              'trophy_type' => $this->entity->id(),
              'trophy_condition' => $key,
            ]
        ),
      ];
      $form['conditions'][$key]['operations'] = [
        '#type' => 'operations',
        '#links' => $links,
      ];
    }

    // Build the new trophy condition addition form and add it to the condition list.
    $new_condition_options = [];
    $conditions = $this->trophyConditionManager->getDefinitions();
    uasort(
          $conditions, function ($a, $b) {
              return Unicode::strcasecmp($a['label'], $b['label']);
          }
      );

    foreach ($conditions as $condition => $definition) {
      $new_condition_options[$condition] = $definition['label'];
    }

    $form['conditions']['new'] = [
      '#tree' => FALSE,
      '#weight' => $user_input['weight'] ?? NULL,
      '#attributes' => ['class' => ['draggable']],
    ];

    $form['conditions']['new']['condition'] = [
      'data' => [
        'new' => [
          '#type' => 'select',
          '#title' => $this->t('Condition'),
          '#title_display' => 'invisible',
          '#options' => $new_condition_options,
          '#empty_option' => $this->t('Select a new condition'),
        ],
        [
          'add' => [
            '#type' => 'submit',
            '#value' => $this->t('Add'),
            '#validate' => ['::conditionValidate'],
            '#submit' => ['::submitForm', '::conditionSave'],
          ],
        ],
      ],
      '#prefix' => '<div class="trophy-condition-new">',
      '#suffix' => '</div>',
    ];
    $form['conditions']['new']['weight'] = [
      '#type' => 'weight',
      '#title' => $this->t('Weight for new condition'),
      '#title_display' => 'invisible',
      '#default_value' => count($this->entity->getConditions()) + 1,
      '#attributes' => ['class' => ['trophy-condition-order-weight']],
    ];
    $form['conditions']['new']['operations'] = [
      'data' => [],
    ];

    $form['summary'] = [
      '#type' => 'trophy_type_summary',
      '#trophy_type' => $this->entity,
      '#weight' => 9,
    ];

    return $form;
  }

  /**
   * Validate handler for trophy condition.
   */
  public function conditionValidate($form, FormStateInterface $form_state) {
    if (!$form_state->getValue('new')) {
      $form_state->setErrorByName('new', $this->t('Select a condition to add.'));
    }
  }

  /**
   * Submit handler for trophy condition.
   */
  public function conditionSave($form, FormStateInterface $form_state) {
    $this->save($form, $form_state);

    // Check if this field has any configuration options.
    $condition = $this->trophyConditionManager->getDefinition($form_state->getValue('new'));

    // Load the configuration form for this option.
    if (is_subclass_of($condition['class'], '\Drupal\trophy\ConfigurableTrophyConditionInterface')) {
      // https://www.drupal.org/project/drupal/issues/2950883#
      if (!is_null($this->requestStack)) {
        $this->requestStack->getCurrentRequest()->query->remove('destination');
      }
      $form_state->setRedirect(
            'trophy.condition_add_form',
            [
              'trophy_type' => $this->entity->id(),
              'trophy_condition' => $form_state->getValue('new'),
            ],
            ['query' => ['weight' => $form_state->getValue('weight')]]
        );
    }
    // If there's no form, immediately add the trophy condition.
    else {
      $condition = [
        'id' => $condition['id'],
        'data' => [],
        'weight' => $form_state->getValue('weight'),
      ];
      $condition_id = $this->entity->addTrophyCondition($condition);
      $this->entity->save();
      if (!empty($condition_id)) {
        $this->messenger()->addStatus($this->t('The trophy condition was successfully applied.'));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    // Update trophy condition weights.
    if (!$form_state->isValueEmpty('conditions')) {
      $this->updateConditionWeights($form_state->getValue('conditions'));
    }

    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $result = parent::save($form, $form_state);
    $message_args = ['%label' => $this->entity->label()];
    $message = $result == SAVED_NEW
        ? $this->t('Created new trophy type %label.', $message_args)
        : $this->t('Updated trophy type %label.', $message_args);
    $this->messenger()->addStatus($message);
    $form_state->setRedirectUrl($this->entity->toUrl('collection'));
    return $result;
  }

  /**
   * Updates trophy condition weights.
   *
   * @param array $conditions
   *   Associative array with conditions having condition uuid as keys and array
   *   with condition data as values.
   */
  protected function updateConditionWeights(array $conditions) {
    foreach ($conditions as $uuid => $condition_data) {
      if ($this->entity->getConditions()->has($uuid)) {
        $this->entity->getCondition($uuid)->setWeight($condition_data['weight']);
      }
    }
  }

}
