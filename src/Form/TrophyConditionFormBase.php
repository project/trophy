<?php

namespace Drupal\trophy\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\SubformState;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\trophy\ConfigurableTrophyConditionInterface;
use Drupal\trophy\TrophyTypeInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Provides a base form for trophy conditions.
 */
abstract class TrophyConditionFormBase extends FormBase {

  /**
   * The trophy type.
   *
   * @var \Drupal\trophy\TrophyTypeInterface
   */
  protected $trophyType;

  /**
   * The trophy condition.
   *
   * @var \Drupal\trophy\TrophyConditionInterface|\Drupal\trophy\ConfigurableTrophyConditionInterface
   */
  protected $trophyCondition;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'trophy_condition_form';
  }

  /**
   * {@inheritdoc}
   *
   * @param array $form
   *   A nested array form elements comprising the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param \Drupal\trophy\TrophyTypeInterface $trophy_type
   *   The trophy type.
   * @param string $trophy_condition
   *   The trophy condition ID.
   *
   * @return array
   *   The form structure.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
   */
  public function buildForm(array $form, FormStateInterface $form_state, TrophyTypeInterface $trophy_type = NULL, $trophy_condition = NULL) {
    $this->trophyType = $trophy_type;
    try {
      $this->trophyCondition = $this->prepareTrophyCondition($trophy_condition);
    }
    catch (PluginNotFoundException $e) {
      throw new NotFoundHttpException("Invalid trophy id: '$trophy_condition'.");
    }
    $request = $this->getRequest();

    if (!($this->trophyCondition instanceof ConfigurableTrophyConditionInterface)) {
      throw new NotFoundHttpException();
    }

    $form['#attached']['library'][] = 'trophy/admin';
    $form['uuid'] = [
      '#type' => 'value',
      '#value' => $this->trophyCondition->getUuid(),
    ];
    $form['id'] = [
      '#type' => 'value',
      '#value' => $this->trophyCondition->getPluginId(),
    ];

    $form['data'] = [];
    $subform_state = SubformState::createForSubform($form['data'], $form, $form_state);
    $form['data'] = $this->trophyCondition->buildConfigurationForm($form['data'], $subform_state);
    $form['data']['#tree'] = TRUE;

    // Check the URL for a weight, then the trophy condition, otherwise use default.
    $form['weight'] = [
      '#type' => 'hidden',
      '#value' => $request->query->has('weight') ? (int) $request->query->get('weight') : $this->trophyCondition->getWeight(),
    ];

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#button_type' => 'primary',
    ];
    $form['actions']['cancel'] = [
      '#type' => 'link',
      '#title' => $this->t('Cancel'),
      '#url' => $this->trophyType->toUrl('edit-form'),
      '#attributes' => ['class' => ['button']],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // The trophy condition configuration is stored
    // in the 'data' key in the form, pass that through for validation.
    $this->trophyCondition->validateConfigurationForm($form['data'], SubformState::createForSubform($form['data'], $form, $form_state));
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_state->cleanValues();

    // The trophy condition configuration is stored
    // in the 'data' key in the form, pass that through for submission.
    $this->trophyCondition->submitConfigurationForm($form['data'], SubformState::createForSubform($form['data'], $form, $form_state));

    $this->trophyCondition->setWeight($form_state->getValue('weight'));
    if (!$this->trophyCondition->getUuid()) {
      $this->trophyType->addTrophyCondition($this->trophyCondition->getConfiguration());
    }
    $this->trophyType->save();

    $this->messenger()->addStatus($this->t('The trophy condition was successfully applied.'));
    $form_state->setRedirectUrl($this->trophyType->toUrl('edit-form'));
  }

  /**
   * Converts an trophy condition ID into an object.
   *
   * @param string $trophy_condition
   *   The trophy condition ID.
   *
   * @return \Drupal\trophy\TrophyConditionInterface
   *   The trophy condition object.
   */
  abstract protected function prepareTrophyCondition($trophy_condition);

}
