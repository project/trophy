<?php

namespace Drupal\trophy\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the trophy entity edit forms.
 */
class TrophyForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $result = parent::save($form, $form_state);

    $entity = $this->getEntity();

    $message_arguments = ['%label' => $entity->toLink()->toString()];
    $logger_arguments = [
      '%label' => $entity->label(),
      'link' => $entity->toLink($this->t('View'))->toString(),
    ];

    switch ($result) {
      case SAVED_NEW:
        $this->messenger()->addStatus($this->t('New trophy %label has been created.', $message_arguments));
        $this->logger('trophy')->notice('Created new trophy %label', $logger_arguments);
        break;

      case SAVED_UPDATED:
        $this->messenger()->addStatus($this->t('The trophy %label has been updated.', $message_arguments));
        $this->logger('trophy')->notice('Updated trophy %label.', $logger_arguments);
        break;
    }

    $form_state->setRedirect('entity.trophy.collection');

    return $result;
  }

}
