<?php

namespace Drupal\trophy\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\trophy\TrophyConditionManager;
use Drupal\trophy\TrophyTypeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides an add form for trophy conditions.
 *
 * @internal
 */
class TrophyConditionAddForm extends TrophyConditionFormBase {

  /**
   * The trophy condition manager.
   *
   * @var \Drupal\trophy\TrophyConditionManager
   */
  protected $conditionManager;

  /**
   * Constructs a new TrophyConditionAddForm.
   *
   * @param \Drupal\trophy\TrophyConditionManager $condition_manager
   *   The trophy condition manager.
   */
  public function __construct(TrophyConditionManager $condition_manager) {
    $this->conditionManager = $condition_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
          $container->get('plugin.manager.trophy_condition')
      );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, TrophyTypeInterface $trophy_type = NULL, $trophy_condition = NULL) {
    $form = parent::buildForm($form, $form_state, $trophy_type, $trophy_condition);
    $form['#title'] = $this->t(
          'Add %condition to %tropy Trophy Type', [
            '%condition' => $this->trophyCondition->label(),
            '%tropy' => $trophy_type->label(),
          ]
      );
    $form['actions']['submit']['#value'] = $this->t('Add trophy');

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function prepareTrophyCondition($trophy_condition) {
    $trophy_condition = $this->conditionManager->createInstance($trophy_condition);
    // Set the initial weight so this trophy comes last.
    $trophy_condition->setWeight(count($this->trophyType->getConditions()));
    return $trophy_condition;
  }

}
