<?php

namespace Drupal\trophy\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\trophy\TrophyTypeInterface;

/**
 * Form for deleting an trophy condition.
 *
 * @internal
 */
class TrophyConditionDeleteForm extends ConfirmFormBase {

  /**
   * The trophy type containing the trophy condition to be deleted.
   *
   * @var \Drupal\trophy\TrophyTypeInterface
   */
  protected $trophyType;

  /**
   * The trophy condition to be deleted.
   *
   * @var \Drupal\trophy\TrophyConditionInterface
   */
  protected $trophyCondition;

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete the @condition from the %trophy trophy type?', [
      '%trophy' => $this->trophyType->label(),
      '@condition' => $this->trophyCondition->label(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return $this->trophyType->toUrl('edit-form');
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'trophy_condition_delete_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, TrophyTypeInterface $trophy_type = NULL, $trophy_condition = NULL) {
    $this->trophyType = $trophy_type;
    $this->trophyCondition = $this->trophyType->getCondition($trophy_condition);

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->trophyType->deleteTrophyCondition($this->trophyCondition);
    $this->messenger()->addStatus($this->t('The trophy condition %name has been deleted.', ['%name' => $this->trophyCondition->label()]));
    $form_state->setRedirectUrl($this->trophyType->toUrl('edit-form'));
  }

}
