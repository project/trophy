<?php

namespace Drupal\trophy\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\trophy\TrophyTypeInterface;

/**
 * Provides an edit form for trophy conditions.
 *
 * @internal
 */
class TrophyConditionEditForm extends TrophyConditionFormBase {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, TrophyTypeInterface $trophy_type = NULL, $trophy_condition = NULL) {
    $form = parent::buildForm($form, $form_state, $trophy_type, $trophy_condition);

    $form['#title'] = $this->t(
          'Edit %condition in %type trophy type', [
            '%condition' => $this->trophyCondition->label(),
            '%type' => $trophy_type->label(),
          ]
      );
    $form['actions']['submit']['#value'] = $this->t('Update trophy');

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function prepareTrophyCondition($trophy_condition) {
    return $this->trophyType->getCondition($trophy_condition);
  }

}
