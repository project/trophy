<?php

namespace Drupal\trophy;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining a trophytype entity type.
 */
interface TrophyTypeInterface extends ConfigEntityInterface {

  /**
   * Returns the trophy type.
   *
   * @return string
   *   The name of the trophy type.
   */
  public function getName();

  /**
   * Sets the name of the trophy type.
   *
   * @param string $name
   *   The name of the trophy type.
   *
   * @return $this
   *   The class instance this method is called on.
   */
  public function setName($name);

  /**
   * Returns the badge of the trophy type.
   *
   * @return id
   *   The file id of the badge.
   */
  public function badge();

  /**
   * Returns the point of the trophy type.
   *
   * @return int
   *   The point of the trophy type.
   */
  public function point();

  /**
   * Returns a specific trophy condition.
   *
   * @param string $condition
   *   The trophy condition UUID.
   *
   * @return \Drupal\trophy\TrophyConditionInterface
   *   The trophy condition.
   */
  public function getCondition($condition);

  /**
   * Returns the trophy conditions for this trophy type.
   *
   * @return \Drupal\trophy\TrophyConditionPluginCollection|\Drupal\trophy\TrophyConditionInterface[]
   *   The trophy condition plugin collection.
   */
  public function getConditions();

  /**
   * Saves an trophy condition for this trophy type.
   *
   * @param array $configuration
   *   An array of trophy condition configuration.
   *
   * @return string
   *   The trophy condition ID.
   */
  public function addTrophyCondition(array $configuration);

  /**
   * Deletes an trophy condition from this trophy type.
   *
   * @param \Drupal\trophy\TrophyConditionInterface $condition
   *   The trophy condition object.
   *
   * @return $this
   */
  public function deleteTrophyCondition(TrophyConditionInterface $condition);

}
