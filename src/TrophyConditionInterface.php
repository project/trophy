<?php

namespace Drupal\trophy;

use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Component\Plugin\DependentPluginInterface;
use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Interface for trophy_condition plugins.
 */
interface TrophyConditionInterface extends PluginInspectionInterface, ConfigurableInterface, DependentPluginInterface {

  /**
   * Computes which trophies should be given to the specified user and gives.
   *
   * @param \Drupal\trophy\Entity\TrophyViewCondition[] $entities
   *   View condition entities.
   * @param int $tid
   *   Trophy Type ID.
   * @param int $uid
   *   User ID.
   */
  public function process($entities, $tid, $uid);

  /**
   * Checks if view condition enabled or not.
   *
   * @return bool
   *   View condition status
   */
  public static function enabled();

  /**
   * Returns the summary of the trophy condition.
   *
   * @return string
   *   The trophy condition Summary.
   */
  public function summary();

  /**
   * Returns the trophy condition plugin label.
   *
   * @return string
   *   The trophy condition label.
   */
  public function label();

  /**
   * Returns the unique ID representing the trophy condition.
   *
   * @return string
   *   The trophy condition ID.
   */
  public function getUuid();

  /**
   * Returns the weight of the trophy condition.
   *
   * @return int|string
   *   Either the integer weight of the trophy condition, or an empty string.
   */
  public function getWeight();

  /**
   * Sets the weight for this trophy condition.
   *
   * @param int $weight
   *   The weight for this trophy condition.
   *
   * @return $this
   */
  public function setWeight($weight);

}
