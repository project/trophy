<?php

namespace Drupal\trophy\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\user\EntityOwnerInterface;
use Drupal\user\EntityOwnerTrait;

/**
 * Defines a Trophy View Condition entity class with no access control handler.
 *
 * @ContentEntityType(
 *   id = "trophy_view_condition",
 *   label = @Translation("Trophy View Condition entity with default access"),
 *   base_table = "trophy_view_condition",
 *   entity_keys = {
 *     "id" = "id",
 *     "owner" = "uid",
 *   }
 * )
 */
class TrophyViewCondition extends ContentEntityBase implements EntityOwnerInterface {
  use EntityOwnerTrait;

  /**
   * {@inheritdoc}
   *
   * Define the field properties here.
   *
   * Field name, type and size determine the table structure.
   *
   * In addition, we can define how the field and its content can be manipulated
   * in the GUI. The behaviour of the widgets used can be determined here.
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);
    $fields += static::ownerBaseFieldDefinitions($entity_type);

    $fields['id'] = BaseFieldDefinition::create('integer')->setReadOnly(TRUE);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default');

    $fields['nid'] = BaseFieldDefinition::create('entity_reference')
      ->setSetting('target_type', 'node')
      ->setSetting('handler', 'default');

    $fields['created'] = BaseFieldDefinition::create('created');

    return $fields;
  }

  /**
   * {@inheritdoc}
   *
   * Define the field properties here.
   *
   * Field name, type and size determine the table structure.
   *
   * In addition, we can define how the field and its content can be manipulated
   * in the GUI. The behaviour of the widgets used can be determined here.
   */
  public static function isAddedRecently($nid, $uid) {
    /**
     * @var \Drupal\Core\Database\Connection
     */
    $connection = \Drupal::service('database');
    $query = $connection->select('trophy_view_condition', 'f')
      ->condition('created', time() - 3600, '>');

    if (!is_null($nid)) {
      $query->condition('nid', $nid);
    }

    if (!is_null($uid)) {
      $query->condition('uid', $uid);
    }

    return $query
      ->countQuery()
      ->execute()
      ->fetchField() != 0;
  }

  /**
   * {@inheritdoc}
   */
  public function getNodeId() {
    return $this->get('nid')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setNodeId($nid) {
    $this->set('nid', $nid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreated() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreated($created) {
    $this->set('created', $created);
    return $this;
  }

}
