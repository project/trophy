<?php

namespace Drupal\trophy\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Entity\EntityWithPluginCollectionInterface;
use Drupal\trophy\TrophyConditionInterface;
use Drupal\trophy\TrophyConditionPluginCollection;
use Drupal\trophy\TrophyTypeInterface;

/**
 * Defines the trophy_type entity type.
 *
 * @ConfigEntityType(
 *   id = "trophy_type",
 *   label = @Translation("Trophy Type"),
 *   label_collection = @Translation("Trophy Types"),
 *   label_singular = @Translation("trophy type"),
 *   label_plural = @Translation("trophy types"),
 *   label_count = @PluralTranslation(
 *     singular = "@count trophy type",
 *     plural = "@count trophy types",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\trophy\TrophyTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\trophy\Form\TrophyTypeForm",
 *       "edit" = "Drupal\trophy\Form\TrophyTypeForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm"
 *     }
 *   },
 *   config_prefix = "trophy_type",
 *   admin_permission = "administer trophy_type",
 *   links = {
 *     "collection" = "/admin/structure/trophy",
 *     "add-form" = "/admin/structure/trophy/add",
 *     "edit-form" = "/admin/structure/trophy/{trophy_type}",
 *     "delete-form" = "/admin/structure/trophy/{trophy_type}/delete"
 *   },
 *   entity_keys = {
 *     "id" = "name",
 *     "uuid" = "uuid",
 *     "label" = "label"
 *   },
 *   config_export = {
 *     "name",
 *     "label",
 *     "description",
 *     "conditions",
 *     "badge",
 *     "point",
 *   }
 * )
 */
class TrophyType extends ConfigEntityBase implements TrophyTypeInterface, EntityWithPluginCollectionInterface {

  /**
   * The name of the trophy type.
   *
   * @var string
   */
  protected $name;

  /**
   * The trophy_type label.
   *
   * @var string
   */
  protected $label;

  /**
   * The trophy_type status.
   *
   * @var bool
   */
  protected $status;

  /**
   * The trophy_type description.
   *
   * @var string
   */
  protected $description;

  /**
   * The trophy_type badge.
   *
   * @var array
   */
  protected $badge;

  /**
   * The trophy_type point.
   *
   * @var int
   */
  protected $point;

  /**
   * The array of trophy conditions for this trophy type.
   *
   * @var array
   */
  protected $conditions = [];

  /**
   * Holds the collection of trophy conditions.
   *
   * That are used by this trophy type.
   *
   * @var \Drupal\trophy\TrophyConditionPluginCollection
   */
  protected $conditionCollection;

  /**
   * {@inheritdoc}
   */
  public function id() {
    return $this->name;
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('point');
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function point() {
    return $this->point;
  }

  /**
   * {@inheritdoc}
   */
  public function badge() {
    return $this->badge;
  }

  /**
   * {@inheritdoc}
   */
  public function getCondition($condition) {
    return $this->getConditions()->get($condition);
  }

  /**
   * {@inheritdoc}
   */
  public function getConditions() {
    if (!$this->conditionCollection) {
      $this->conditionCollection = new TrophyConditionPluginCollection($this->getTrophyConditionPluginManager(), $this->conditions);
      $this->conditionCollection->sort();
    }
    return $this->conditionCollection;
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginCollections() {
    return ['conditions' => $this->getConditions()];
  }

  /**
   * {@inheritdoc}
   */
  public function addTrophyCondition(array $configuration) {
    $configuration['uuid'] = $this->uuidGenerator()->generate();
    $this->getConditions()->addInstanceId($configuration['uuid'], $configuration);
    return $configuration['uuid'];
  }

  /**
   * {@inheritdoc}
   */
  public function deleteTrophyCondition(TrophyConditionInterface $condition) {
    $this->getConditions()->removeInstanceId($condition->getUuid());
    $this->save();
    return $this;
  }

  /**
   * Returns the trophy condition plugin manager.
   *
   * @return \Drupal\Component\Plugin\PluginManagerInterface
   *   The trophy condition plugin manager.
   */
  protected function getTrophyConditionPluginManager() {
    return \Drupal::service('plugin.manager.trophy_condition');
  }

}
