<?php

namespace Drupal\trophy\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\trophy\TrophyInterface;
use Drupal\user\EntityOwnerTrait;

/**
 * Defines the trophy entity class.
 *
 * @ContentEntityType(
 *   id = "trophy",
 *   label = @Translation("Trophy"),
 *   label_collection = @Translation("Trophies"),
 *   label_singular = @Translation("trophy"),
 *   label_plural = @Translation("trophies"),
 *   label_count = @PluralTranslation(
 *     singular = "@count trophies",
 *     plural = "@count trophies",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\trophy\TrophyListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "form" = {
 *       "add" = "Drupal\trophy\Form\TrophyForm",
 *       "edit" = "Drupal\trophy\Form\TrophyForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\trophy\Routing\TrophyHtmlRouteProvider",
 *     }
 *   },
 *   base_table = "trophy",
 *   admin_permission = "administer trophy",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "id",
 *     "condition_uuid" = "condition_uuid",
 *     "owner" = "uid",
 *   },
 *   links = {
 *     "collection" = "/admin/content/trophy",
 *     "add-form" = "/trophy/add",
 *     "canonical" = "/trophy/{trophy}",
 *     "edit-form" = "/trophy/{trophy}",
 *     "delete-form" = "/trophy/{trophy}/delete",
 *   },
 * )
 */
class Trophy extends ContentEntityBase implements TrophyInterface {

  use EntityOwnerTrait;

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);
    if (!$this->getOwnerId()) {
      // If no owner has been set explicitly, make the anonymous user the owner.
      $this->setOwnerId(0);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {

    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Status'))
      ->setRequired(TRUE)
      ->setDefaultValue(TRUE)
      ->setSetting('on_label', 'Enabled')
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'settings' => [
          'display_label' => FALSE,
        ],
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'boolean',
        'label' => 'above',
        'weight' => 0,
        'settings' => [
          'format' => 'enabled-disabled',
        ],
      ])
      ->setRequired(TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['tid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Trophy Type'))
      ->setRequired(TRUE)
      ->setSetting('target_type', 'trophy_type')
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => 60,
          'placeholder' => '',
        ],
        'weight' => 15,
      ]);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Owner'))
      ->setRequired(TRUE)
      ->setSetting('target_type', 'user')
      ->setDefaultValueCallback(static::class . '::getDefaultEntityOwner')
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => 60,
          'placeholder' => '',
        ],
        'weight' => 15,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'author',
        'weight' => 15,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Authored on'))
      ->setRequired(TRUE)
      ->setDescription(t('The time that the trophy was created.'))
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'datetime_timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['acquisition_date'] = BaseFieldDefinition::create('timestamp')
      ->setRequired(TRUE)
      ->setInternal(TRUE);

    $fields['condition_uuid'] = BaseFieldDefinition::create('string')
      ->setRequired(TRUE)
      ->setInternal(TRUE);

    return $fields;
  }

}
