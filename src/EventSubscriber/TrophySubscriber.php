<?php

namespace Drupal\trophy\EventSubscriber;

use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\Routing\AdminContext;
use Drupal\trophy\TrophyConditionManager;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Trophy event subscriber.
 */
class TrophySubscriber implements EventSubscriberInterface {

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The Trophy Condition Manager.
   *
   * @var \Drupal\trophy\TrophyConditionManager
   */
  protected $trophyConditionManager;

  /**
   * The Admin Context.
   *
   * @var \Drupal\Core\Routing\AdminContext
   */
  protected $adminContext;

  /**
   * The queue factory.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queue;

  /**
   * Constructs event subscriber.
   *
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   * @param \Drupal\trophy\TrophyConditionManager $trophy_condition_manager
   *   The trophy condition manager.
   * @param \Drupal\Core\Routing\AdminContext $admin_context
   *   The admin context.
   * @param \Drupal\Core\Queue\QueueFactory $queue
   *   The queue factory.
   */
  public function __construct(MessengerInterface $messenger, TrophyConditionManager $trophy_condition_manager, AdminContext $admin_context, QueueFactory $queue) {
    $this->messenger = $messenger;
    $this->trophyConditionManager = $trophy_condition_manager;
    $this->adminContext = $admin_context;
    $this->queue = $queue;
  }

  /**
   * Kernel request event handler.
   *
   * It will call every Trophy Condition's handlers.
   *
   * @param \Symfony\Component\HttpKernel\Event\RequestEvent $event
   *   Request event.
   */
  public function onKernelRequest(RequestEvent $event) {
    if ($event->isMainRequest() && !$this->adminContext->isAdminRoute()) {
      foreach ($this->trophyConditionManager->getDefinitions() as $definition) {
        if (($definition['class'] . '::enabled')()) {
          ($definition['class'] . '::onKernelRequest')($event, $this->queue);
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      KernelEvents::REQUEST => ['onKernelRequest', 280],
    ];
  }

}
