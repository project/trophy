<?php

namespace Drupal\trophy\Element;

use Drupal\Core\Render\Element\InlineTemplate;
use Drupal\trophy\Entity\TrophyType;

/**
 * Provides a render element where the user supplies an in-line Twig template.
 *
 * Properties:
 * - #trophy_type: TrophyType Entity.
 *
 * Usage example:
 *
 * @code
 *   $form['summary'] = [
 *     '#type' => 'trophy_type_summary',
 *     '#trophy_type' => $this->entity
 *   ];
 * @endcode
 *
 * @RenderElement("trophy_type_summary")
 */
class TrophyTypeSummary extends InlineTemplate {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = static::class;
    return [
      '#pre_render' => [
      [$class, 'preRenderInlineTemplate'],
      ],
      '#template' => '',
      '#context' => [],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function preRenderInlineTemplate($element) {
    $trophyType = $element['#trophy_type'];
    if (empty($trophyType) || !$trophyType instanceof TrophyType) {
      return $element;
    }

    $template = "<strong>Summary: </strong>";
    $t = \Drupal::translation();

    if ($trophyType->getConditions()->count() == 0) {
      $template .= $t->translate('This Trophy Type will not work. It requires at least one condition to work.')->render();
    }
    else {
      $template .= $t->translate(
            "The Trophy Entity worth %point points will be assigned to the user when@precondition any of the following conditions are met:", [
              '%point' => $trophyType->point(),
              '@precondition' => !$trophyType->status() ? ' ' . $t->translate("enabled") . ' ' . $t->translate("and") : '',
            ]
        )->render();

      $template .= '<ul>';
      foreach ($trophyType->getConditions() as $condition) {
        $template .= "<li>{$condition->summary()}</li>";
      }
      $template .= '</ul>';
    }

    $element['#template'] = $template;
    $element = parent::preRenderInlineTemplate($element);

    return $element;
  }

}
