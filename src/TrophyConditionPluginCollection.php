<?php

namespace Drupal\trophy;

use Drupal\Core\Plugin\DefaultLazyPluginCollection;

/**
 * A collection of trophy conditions.
 */
class TrophyConditionPluginCollection extends DefaultLazyPluginCollection {

  /**
   * {@inheritdoc}
   */
  public function sortHelper($aID, $bID) {
    return $this->get($aID)->getWeight() <=> $this->get($bID)->getWeight();
  }

}
