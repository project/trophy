<?php

namespace Drupal\trophy\Plugin\QueueWorker;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\Core\State\StateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\trophy\Plugin\TrophyCondition\ViewCondition;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines 'trophy_view_queue' queue worker.
 *
 * @QueueWorker(
 *   id = "trophy_view_queue",
 *   title = @Translation("TrophyViewQueue"),
 *   cron = {"time" = 60}
 * )
 */
class TrophyViewQueue extends QueueWorkerBase implements ContainerFactoryPluginInterface {


  use StringTranslationTrait;
  use MessengerTrait;

  /**
   * The state.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The Entity Type Manager Interface.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The logger.
   *
   * @var \Drupal\trophy\Entity\TrophyViewCondition[]
   */
  protected $entities;

  /**
   * ReportWorkerBase constructor.
   *
   * @param array $configuration
   *   The configuration of the instance.
   * @param string $plugin_id
   *   The plugin id.
   * @param mixed $plugin_definition
   *   The plugin definition.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state service the instance should use.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger
   *   The logger service the instance should use.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The Entity Type Manager service the instance should use.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, StateInterface $state, LoggerChannelFactoryInterface $logger, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->state = $state;
    $this->logger = $logger->get('trophy');
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $form = new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('state'),
      $container->get('logger.factory'),
      $container->get('entity_type.manager')
    );
    $form->setMessenger($container->get('messenger'));
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($uid) {
    /**
     * @var \Drupal\trophy\Entity\TrophyType
     */
    foreach ($this->entityTypeManager->getStorage('trophy_type')->loadByProperties([]) as $trophyType) {
      if (!$trophyType->enable()) {
        continue;
      }

      $conditions = iterator_to_array($trophyType->getConditions()->getIterator());
      $conditions = array_filter($conditions, function ($condition) {
        return $condition instanceof ViewCondition;
      });

      if (count($conditions) == 0) {
        continue;
      }

      if (is_null($this->entities)) {
        $this->entities = $this->entityTypeManager->getStorage('trophy_view_condition')->loadByProperties([]);
      }

      foreach ($conditions as $condition) {
        $condition->process($this->entities, $trophyType->id(), $uid);
      }
    }

    if ($this->state->get('trophy_cron_show_status_message')) {
      $this->messenger()->addMessage(
        $this->t('@worker worker processed item for user @user', [
          '@worker' => self::class,
          '@user' => $uid,
        ])
      );
    }
    $this->logger->info('@worker worker processed item for user @user', [
      '@worker' => self::class,
      '@user' => $uid,
    ]);
  }

}
