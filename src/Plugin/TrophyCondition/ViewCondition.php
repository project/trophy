<?php

namespace Drupal\trophy\Plugin\TrophyCondition;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\node\Entity\Node;
use Drupal\trophy\ConfigurableTrophyConditionBase;
use Drupal\trophy\Entity\TrophyViewCondition;
use Symfony\Component\HttpKernel\Event\RequestEvent;

/**
 * Plugin implementation of the trophy_condition.
 *
 * @TrophyCondition(
 *   id = "view_condition",
 *   label = @Translation("View Condition"),
 *   description = @Translation("View Condition.")
 * )
 */
class ViewCondition extends ConfigurableTrophyConditionBase {

  /**
   * {@inheritdoc}
   */
  public static function enabled() {
    return count(\Drupal::entityTypeManager()->getStorage('trophy_type')->loadByProperties(['status' => TRUE])) != 0;
  }

  /**
   * {@inheritdoc}
   */
  public static function onKernelRequest(RequestEvent $event, QueueFactory $queue) {
    if ($event->getRequest()->getSession()->has('uid')) {
      $nid = $event->getRequest()->attributes->get('node')?->id();
      $uid = $event->getRequest()->getSession()->get('uid');

      if (TrophyViewCondition::isAddedRecently($nid, $uid)) {
        return;
      }

      $trophyViewConditionEntity = TrophyViewCondition::create([
        'nid' => $nid,
        'uid' => $uid,
      ]);

      if ($trophyViewConditionEntity->validate()->count() == 0) {
        $trophyViewConditionEntity->save();
        $queue->get('trophy_view_queue')->createItem($uid);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function process($entities, $tid, $uid) {
    $lowest = 0;
    $current = 0;
    $justCreated = FALSE;
    $latestTrophy = $this->entityTypeManager->getStorage('trophy')->getQuery()->accessCheck()->condition('uid', $uid)->condition('tid', $tid)->condition('condition_uuid', $this->getUuid())->sort('created', 'DESC')->range(0, 1)->execute();
    $latestTrophy = reset($latestTrophy);
    $target = $this->configuration['target'];
    $recursively = $this->configuration['recursively'];
    $requireConsistency = $this->configuration['require_consistency'];

    if ($latestTrophy !== FALSE) {
      if (empty($recursively) || $recursively == FALSE) {
        return;
      }

      /**
       * @var \Drupal\trophy\Entity\Trophy
       */
      $trophy = $this->entityTypeManager->getStorage('trophy')->load($latestTrophy);
      $lowest = $trophy->get('acquisition_date')->getValue();
      $justCreated = TRUE;
    }

    $entities = array_filter($entities, function (TrophyViewCondition $entity) use ($uid, $lowest) {
      $created = $entity->getCreated();
      return $created >= $lowest
       && $entity->getOwnerId() == $uid
       && ($created >= $this->configuration['from'] && $created <= $this->configuration['to'])
       && (count($this->configuration['nodes']) == 0 || in_array($entity->getNodeID(), $this->configuration['nodes']));
    });

    $entities = array_map(function (TrophyViewCondition $entity) {
      return $entity->getCreated();
    }, $entities);
    sort($entities);

    if ($this->configuration['granularity'] == 'm') {
      foreach ($entities as $entity) {
        $y = intval((new \DateTime('@' . $entity))->format('y')) * 12;
        $m = intval((new \DateTime('@' . $entity))->format('m'));
        $entity = $y + $m;

        if ($entity == $lowest) {
          continue;
        }

        $current = $requireConsistency ? ($entity == $lowest + 1 ? $current + 1 : 0) : ($entity >= $lowest + 1 ? $current + 1 : $current);
        $lowest = $entity;

        if ($current == $target) {
          $this->entityTypeManager->getStorage('trophy')->create([
            'uid' => $uid,
            'tid' => $tid,
            'acquisition_date' => $lowest,
            'condition_uuid' => $this->getUuid(),
          ])->save();

          $current = 0;

          if (empty($recursively) || $recursively == FALSE) {
            break;
          }
        }
      }
    }
    else {
      // xdebug_break();
      $padding = match($this->configuration['granularity']){
        'h' => 3600,
        'd' => 86400,
        'w' => 604800,
      };
      foreach ($entities as $entity) {
        if ($entity > $lowest + $padding || (!$justCreated && $entity >= $lowest + $padding)) {
          if (!$justCreated && $lowest != 0 && $requireConsistency && $entity >= $lowest + $padding * 2) {
            $lowest = $entity;
            $current = 0;
            continue;
          }

          $justCreated = FALSE;
          $lowest = $entity;
          $current += 1;

          if ($current == $target) {
            $this->entityTypeManager->getStorage('trophy')->create([
              'uid' => $uid,
              'tid' => $tid,
              'acquisition_date' => $lowest,
              'condition_uuid' => $this->getUuid(),
            ])->save();

            $current = 0;
            $justCreated = TRUE;

            if (empty($recursively) || $recursively == FALSE) {
              break;
            }
          }
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function summary() {
    $readables = [
      'h' => $this->t('hour'),
      'd' => $this->t('day'),
      'w' => $this->t('week'),
      'm' => $this->t('month'),
      'y' => $this->t('year'),
    ];

    $from = $this->dateFormatter->format($this->configuration['from'], 'long');

    if ($this->configuration['to'] == NULL) {
      $date = $this->t("After <i>@from</i>", [
        '@from' => $from,
      ]);
    }
    else {
      $date = $this->t("Between <i>@from</i> and <i>@to</i>", [
        '@from' => $from,
        '@to' => $this->dateFormatter->format($this->configuration['to'], 'long'),
      ]);
    }

    $target = $this->configuration['target'];
    $granularity = $readables[$this->configuration['granularity']]->render();
    $requireConsistency = $this->configuration['require_consistency'] == TRUE ? $this->t('if they visit <b>continuously</b>') : '';
    $recursively = empty($this->configuration['recursively']) ? $this->t('once') : $this->t('each @step', [
      '@step' => $readables[$this->configuration['recursively']]->render(),
    ]);
    $nodes = empty($this->configuration['nodes']) ? $this->t('any pages') : $this->t('@nodes nodes', [
      '@nodes' => implode(', ', $this->configuration['nodes']),
    ]);

    return $this->t("@date the user will earn a point each <b>@granularity</b> when they visit <b>@nodes</b> @requireConsistency. Whoever reaches <b>@target</b> points will earn <u>a Trophy</u> <b>@recursively</b>.", [
      '@date' => $date,
      '@granularity' => $granularity,
      '@nodes' => $nodes,
      '@requireConsistency' => $requireConsistency,
      '@target' => $target,
      '@recursively' => $recursively,
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'from' => new DrupalDateTime(),
      'to' => (new DrupalDateTime())->add(\DateInterval::createFromDateString('3 day')),
      'target' => 3,
      'granularity' => 'd',
      'require_consistency' => FALSE,
      'recursively' => NULL,
      'nodes' => [],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    if (is_numeric($this->configuration['from'])) {
      $this->configuration['from'] = DrupalDateTime::createFromTimestamp($this->configuration['from']);
    }

    if (is_numeric($this->configuration['to'])) {
      $this->configuration['to'] = DrupalDateTime::createFromTimestamp($this->configuration['to']);
    }

    $form['from'] = [
      '#type' => 'datetime',
      '#title' => 'From',
      '#date_increment' => 1,
      '#date_timezone' => date_default_timezone_get(),
      '#default_value' => $this->configuration['from'],
      '#description' => $this->t('Date time, #type = datetime'),
      '#required' => TRUE,
    ];

    $form['to'] = [
      '#type' => 'datetime',
      '#title' => 'To',
      '#date_increment' => 1,
      '#date_timezone' => date_default_timezone_get(),
      '#default_value' => $this->configuration['to'],
      '#description' => $this->t('Date time, #type = datetime'),
    ];

    $form['target'] = [
      '#type' => 'number',
      '#title' => $this->t('Target'),
      '#description' => $this->t('Number, #type = number'),
      '#default_value' => $this->configuration['target'],
      '#required' => TRUE,
      '#min' => 1,
    ];

    $form['granularity'] = [
      '#type' => 'select',
      '#title' => $this->t('Granularity'),
      '#options' => [
        'h' => $this->t('Hours'),
        'd' => $this->t('Days'),
        'w' => $this->t('Weeks'),
        'm' => $this->t('Months'),
      ],
      '#default_value' => $this->configuration['granularity'],
      '#empty_option' => $this->t('-select-'),
      '#description' => $this->t('Select, #type = select'),
      '#required' => TRUE,
    ];

    $form['recursively'] = [
      '#type' => 'select',
      '#title' => $this->t('Recursively'),
      '#options' => [
        'd' => $this->t('Daily'),
        'w' => $this->t('Weekly'),
        'm' => $this->t('Monthly'),
        'y' => $this->t('Yearly'),
      ],
      '#default_value' => $this->configuration['recursively'],
      '#empty_option' => $this->t('-select-'),
      '#description' => $this->t('Select, #type = select'),
    ];

    $form['adv_limitations'] = [
      '#type' => 'details',
      '#title' => 'Advanced Limitations',
    ];

    $form['adv_limitations']['require_consistency'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Require Consistency'),
      '#default_value' => $this->configuration['require_consistency'],
    ];

    $form['adv_limitations']['nodes'] = [
      '#type' => 'entity_autocomplete',
      '#target_type' => 'node',
      '#title' => $this->t('Choose nodes (Separate with commas)'),
      '#tags' => TRUE,
      '#default_value' => Node::loadMultiple($this->configuration['nodes']),
      '#selection_handler' => 'default',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $this->configuration['from'] = $form_state->getValue('from')->getTimestamp();
    if ($form_state->hasValue('to')) {
      $this->configuration['to'] = $form_state->getValue('to')->getTimestamp();
    }
    else {
      $this->configuration['to'] = NULL;
    }
    $this->configuration['target'] = $form_state->getValue('target');
    $this->configuration['granularity'] = $form_state->getValue('granularity');
    $this->configuration['recursively'] = $form_state->getValue('recursively');
    $this->configuration['require_consistency'] = $form_state->getValue('adv_limitations')['require_consistency'];
    if (is_array($form_state->getValue('adv_limitations')['nodes'])) {
      $this->configuration['nodes'] = array_column($form_state->getValue('adv_limitations')['nodes'], 'target_id');
    }
    else {
      $this->configuration['nodes'] = [];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::validateConfigurationForm($form, $form_state);
    if ($form_state->hasAnyErrors()) {
      return;
    }
    if ($form_state->hasValue('to')) {
      $start_date = $form_state->getValue('from')->getTimestamp();
      $end_date = $form_state->getValue('to')->getTimestamp();

      if ($end_date < $start_date) {
        $form_state->setErrorByName('to', $this->t('The to date must be greater than the from date.'));
      }
    }

    $values = ['h', 'd', 'w', 'm', 'y'];
    $recursivelyIndex = array_search($form_state->getValue('recursively'), $values);
    if ($recursivelyIndex !== FALSE) {
      $granularityIndex = array_search($form_state->getValue('granularity'), $values);
      if ($recursivelyIndex < $granularityIndex) {
        $form_state->setErrorByName(['recursively'], $this->t('Recursion value must be more extensive or equal than the granularity value'));
      }
    }
  }

}
