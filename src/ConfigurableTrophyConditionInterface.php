<?php

namespace Drupal\trophy;

use Drupal\Core\Plugin\PluginFormInterface;

/**
 * Defines the interface for configurable trophy conditions.
 *
 * @see plugin_api
 */
interface ConfigurableTrophyConditionInterface extends TrophyConditionInterface, PluginFormInterface {
}
