<?php

namespace Drupal\trophy;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining a trophy entity type.
 */
interface TrophyInterface extends ContentEntityInterface, EntityOwnerInterface {

}
