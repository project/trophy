<?php

namespace Drupal\trophy;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines the access control handler for the trophy entity type.
 */
class TrophyAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {

    switch ($operation) {
      case 'view':
        return AccessResult::allowedIfHasPermission($account, 'view trophy');

      case 'update':
        return AccessResult::allowedIfHasPermissions(
            $account,
            ['edit trophy', 'administer trophy'],
            'OR',
        );

      case 'delete':
        return AccessResult::allowedIfHasPermissions(
            $account,
            ['delete trophy', 'administer trophy'],
            'OR',
        );

      default:
        // No opinion.
        return AccessResult::neutral();
    }

  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermissions(
          $account,
          ['create trophy', 'administer trophy'],
          'OR',
      );
  }

}
