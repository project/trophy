<?php

namespace Drupal\trophy;

use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a base class for configurable trophy conditions.
 *
 * @see plugin_api
 */
abstract class ConfigurableTrophyConditionBase extends TrophyConditionBase implements ConfigurableTrophyConditionInterface {

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
  }

}
