<?php

namespace Drupal\trophy;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * TrophyCondition plugin manager.
 */
class TrophyConditionManager extends DefaultPluginManager {

  /**
   * Constructs TrophyConditionManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
          'Plugin/TrophyCondition',
          $namespaces,
          $module_handler,
          'Drupal\trophy\TrophyConditionInterface',
          'Drupal\trophy\Annotation\TrophyCondition'
      );
    $this->alterInfo('trophy_condition_info');
    $this->setCacheBackend($cache_backend, 'trophy_condition_plugins');
  }

}
